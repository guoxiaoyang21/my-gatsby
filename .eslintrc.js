/*
 * @Author: Guo Xiaoyang
 * @Date: 2020-09-21 14:52:00
 * @LastEditTime: 2020-09-21 14:52:07
 * @LastEditors: Guo Xiaoyang
 * @Description: 
 * @FilePath: /my-gatsby/.eslintrc.js
 * @Copyright 2020 guoxiaoyang <guoxiaoyang@shimo.im>. All rights reserved.
 */
module.exports = {
  extends: [
    'alloy',
  ],
  env: {
    // Your environments (which contains several predefined global variables)
    //
    // browser: true,
    // node: true,
    // mocha: true,
    // jest: true,
    // jquery: true
  },
  globals: {
    // Your global variables (setting to false means it's not allowed to be reassigned)
    //
    // myGlobal: false
  },
  rules: {
    // Customize your rules
  },
};