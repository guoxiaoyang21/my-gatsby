/*
 * @Author: Guo Xiaoyang
 * @Date: 2020-09-21 13:48:17
 * @LastEditTime: 2020-09-21 14:05:36
 * @LastEditors: Guo Xiaoyang
 * @Description: 
 * @FilePath: /my-gatsby/src/pages/page-2.js
 * @Copyright 2020 guoxiaoyang <guoxiaoyang@shimo.im>. All rights reserved.
 */
import React from 'react'
import Link from 'gatsby-link'

const SecondPage = () => (
  <div>
    <h1>Hi from the second page</h1>
    <p>Welcome to page 2</p>
    <Link to="/">Go back to the homepage</Link>
  </div>
)

var j = 0;
var c = 1;
while (true) { // Noncompliant; constant end condition
  j++;
}


export default SecondPage
